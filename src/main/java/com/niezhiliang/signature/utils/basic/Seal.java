package com.niezhiliang.signature.utils.basic;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public interface Seal {

    /**
     * 绘制圆形
     */
     void drawCircle();

    /**
     * 绘制椭圆
     */
     void drawEllipse();

    /**
     * 初始化BufferedImage
     */
    void initBufferedImage();

}
