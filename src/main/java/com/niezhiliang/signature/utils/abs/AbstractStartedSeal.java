package com.niezhiliang.signature.utils.abs;

import com.niezhiliang.signature.utils.basic.AbstractBasicSeal;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * 创建抽象产品类,定义抽象产品的公共接口
 *
 */
public abstract class AbstractStartedSeal extends AbstractBasicSeal {

    @Override
    public abstract void drawCircle();

    @Override
    public abstract void drawEllipse();
}
