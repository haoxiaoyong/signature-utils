package com.niezhiliang.signature.utils.container;


import com.niezhiliang.signature.utils.abs.AbstractSimpleSeal;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 *
 * 创建具体产品类（继承抽象产品类,定义生产的具体产品)
 */
public class SimpleSeal extends AbstractSimpleSeal {

    @Override
    public void drawCircle() {

    }

    @Override
    public void drawEllipse() {

    }
}
